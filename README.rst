.. -*- restructuredtext -*-

=====================================
Disjoint Compatible Perfect Matchings
=====================================
This repository contains the complete sources of the Webpage:
http://www.opimedia.be/CV/2016-2017-ULB/INFO-F420-Computational-geometry/Project-Disjoint-Compatible-Perfect-Matchings/

This is a project of *INFO-F420 Computational geometry* course (17.5/20),
to present *Disjoint Compatible Perfect Matchings*, with a JavaScript application to experiment.

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2017 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|Disjoint Compatible Perfect Matchings|

.. |Disjoint Compatible Perfect Matchings| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/15/3338367369-5-disjoint-compatible-perfect-matchings_avatar.png
