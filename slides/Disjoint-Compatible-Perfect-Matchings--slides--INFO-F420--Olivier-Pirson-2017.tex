% !TeX spellcheck = en
% -*- coding: utf-8 -*-
\documentclass[a4paper,ps2pdf,9pt]{beamer}
\usepackage{slides}

\title{Disjoint Compatible Perfect Matchings --- slides}

\author{\href{http://www.opimedia.be/}{Olivier \textsc{Pirson}}}
\institute{\href{http://homepages.ulb.ac.be/~slanger/cg/}{INFO-F420 \textit{Computational geometry}}\\
  \href{http://www.ulb.ac.be/}{\includegraphics[height=0.75cm]{img/ULB}}
}
\date{April 26, 2017\\
  \tiny(Some corrections November 26, 2017)}

\def\EMAILOPi{olivier.pirson.opi@gmail.com}

\hypersetup{pdfmoddate={D:20171126}}
\hypersetup{pdfauthor={Olivier Pirson <\EMAILOPi>}}
\hypersetup{pdfsubject={Presentation of Disjoint Compatible Perfect Matchings, with a JavaScript application to experiment.}}
\hypersetup{pdfkeywords={computational geometry, graph, matching, perfect matching, compatible matchings, disjoint matchings}}
\hypersetup{pdfcreationdate={D:20170426}}

\logo{\includegraphics[width=1cm]{img/Disjoint-Compatible-Perfect-Matchings}}


\hyphenpenalty=10000
\sloppy

\begin{document}
\title{\includegraphics[width=1.5cm]{img/Disjoint-Compatible-Perfect-Matchings}\\[1cm]
  \href{http://www.opimedia.be/CV/2016-2017-ULB/INFO-F420-Computational-geometry/Project-Disjoint-Compatible-Perfect-Matchings/}{{\Huge\textit{Disjoint Compatible Perfect Matchings}}}}
\begin{frame}[plain]
  \null\hspace*{-2em}%
  \parbox{\textwidth}{\titlepage}%
\end{frame}
\title{\textit{Disjoint Compatible Perfect Matchings}}
\author{}
\institute{}
\date{}

\AtBeginSection{
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}



\section[Definitions]{Some basic definitions}
\begin{frame}[t]
  \frametitle{Planar straight line graph (PSLG)}

  \begin{figure}
    \includegraphics[height=2cm]{img/planar-straight-line-graph}
  \end{figure}
  A \textit{planar straight line graph}
  \begin{itemize}
  \item
    is an undirected graph
  \item
    each vertex is a point in the plane
  \item
    each edge is a segment between two points (no curve)
  \item
    no segment intersection
  \end{itemize}
\end{frame}


\begin{frame}[t]
  \frametitle{Perfect matching}

  \begin{figure}
    \includegraphics[height=2cm]{img/matching}
  \end{figure}
  A \textit{matching}
  is a set of segments with no point in common\\
  (each vertex has degree at most one)

  \begin{figure}
    \includegraphics[height=2cm]{img/perfect-matching}
  \end{figure}
  A matching if \textit{perfect}
  if and only if each vertex has degree one
\end{frame}


\begin{frame}[t]
  \frametitle{Canonical perfect matching}

  \begin{figure}
    \includegraphics[height=2cm]{img/canonical-perfect-matching}
  \end{figure}
  \begin{itemize}
  \item
  $S$ a set of $2n$ points
  \item
    $p_1, p_2, p_3, \dots, p_{2n}$
    in increasing order of their x-coordinates\\
    (and if necessary of their y-coordinates),
  \end{itemize}
  The \textit{canonical perfect matching} of $S$, writed $N(S)$,\\
  is the perfect matching with segments\\
  $p_1$---$p_2, p_3$---$p_4, p_5$---$p_6, \dots p_{2n-1}$---$p_{2n}$.
\end{frame}


\begin{frame}[t]
  \frametitle{Compatible perfect matchings}

  Consider now two perfect matchings.

  \medskip
  Two perfect matchings are \textit{compatible}\\
  if and only if their union is with no intersection.

  \bigskip
  \begin{figure}
    \includegraphics[height=2cm]{img/not-compatible-perfect-matchings}
    \caption{These two perfect matchings are \textbf{not} compatible.}
  \end{figure}

  \bigskip
  Be careful, the union is the union of two sets (concept of set theory).\\
  The intersection is the intersection of two segments (geometrical concept).
\end{frame}


\begin{frame}[t]
  \frametitle{Transformation between two perfect matchings}

  \begin{figure}
    \includegraphics[height=2cm]{img/transformation}
    \caption{Transformation of length $2$}
  \end{figure}
  \mbox{}\\[-6ex]
  \begin{itemize}
    \item
      $S$ a set of points
    \item
      $M$ and $M'$ two perfect matchings of $S$
    \item
      a \textit{transformation} between $M$ and $M'$ of length $k$
      is a sequence $M = M_0, M_1, M_2, \dots, M_k = M'$ of perfect matchings of $S$
    \item
      such that $\forall i: M_i$ and $M_{i+1}$ are compatible
  \end{itemize}

  \begin{theorem}
    $\forall$ perfect matchings $M$ and $M'$,\\
    $\exists$ transformation of length at most
    $2 \lceil\lg(n)\rceil$ between $M$ and $M'$
  \end{theorem}
\end{frame}



\section[Proof]{Sketch of proof}
\begin{frame}[t]
  \frametitle{Lemma \romannum{1}}

  \begin{figure}
    \includegraphics[height=2cm]{img/lemma-i}
  \end{figure}

  \begin{lemma}
    $\forall$ perfect matching $M$,\\
    $\forall$ line $t$ cutting an even number of segments of $M$
    ($t$ contains no vertex),\\
    let $H$ the halfplane determined by $t$,\\
    let $S$ the set of vertices of $M$ in $H$,\\
    $\exists$ perfect matching $M'$ of $S:
    M$ and $M'$ are compatible
  \end{lemma}
\end{frame}


\begin{frame}[t]
  \frametitle{Lemma \romannum{2}}

  \mbox{}\\[-5.5ex]
  \begin{figure}
    \includegraphics[height=2cm]{img/lemma-ii}
  \end{figure}

  \mbox{}\\[-4ex]
  \begin{lemma}
    $\forall$ perfect matching $M$,\\
    $\forall$ line $t$ cutting an even number of segments of $M$
    ($t$ contains no vertex),\\
    let halfplanes $H_1$ and $H_2$ determined by $t$,\\
    let $S_1$ and $S_2$ sets of vertices of $M$ in $H_1$ and in $H_2$,\\
    $\exists$ perfect matchings $M_1$ of $S_1$ and $M_2$ of $S_2:
    M$ and $(M_1 \cup M_2)$ are compatible
  \end{lemma}

  \begin{proof}
    \begin{itemize}
    \item
      by lemma \romannum{1},
      $\exists$ perfect matchings $M_1$ of $S_1$ and $M_2$ of $S_2:$\\
      $M$ and $M_1$ are compatible,
      and $M$ and $M_2$ are compatible
    \item
      $M_1$ and $M_2$ are separated,\\
      thus $M_1 \cup M_2$ is a perfect matching
      compatible with $M$
    \end{itemize}
  \end{proof}
\end{frame}


\begin{frame}[t]
  \frametitle{Lemma \romannum{3}}

  \begin{lemma}
    $\forall S$ of $2n$ points,\\
    $\forall$ perfect matchings $M$ of $S$,\\
    $\exists$ transformation of length at most
    $\lceil\lg(n)\rceil$
    between $M$ and $N(S)$
  \end{lemma}

  \begin{proof}
    With $S$ set of $2n$ points,
    proof by induction on $n$.
    \begin{figure}
      \includegraphics[height=2cm]{img/lemma-iii}
    \end{figure}
    \mbox{}\\[-6ex]
    \begin{itemize}
    \item
      Cut the plane in two
      and apply lemma \romannum{2}\ on each half.
    \item
      Union of transformation of each parts.
    \end{itemize}
  \end{proof}
\end{frame}


\begin{frame}[t]
  \frametitle{Theorem}

  \begin{theorem}
    $\forall$ perfect matchings $M$ and $M'$,\\
    $\exists$ transformation of length at most
    $2 \lceil\lg(n)\rceil$ between $M$ and $M'$
  \end{theorem}

  \begin{proof}
    $S$ the set of $2n$ points.\\
    By lemma \romannum{3},
    $\exists$ perfect matchings $M$ and $M':$\\
    \qquad
    $M = M_0, M_1, M_2, \dots, M_k = N(S)$ and\\
    \qquad
    $M' = M'_0, M'_1, M'_2, \dots, M'_{k'} = N(S)$
    with $k, k' \leq \lceil\lg(n)\rceil$.

    Thus $M_0, M_1, M_2, \dots, M_k = M'_{k'}, \dots, M'_2, M'_1, M'_0 = M'$
    is a transformation of length at most $2 \lceil\lg(n)\rceil$.
  \end{proof}
\end{frame}



\section{Web page}
\begin{frame}
  \frametitle{Web page and demonstration of the application}

  \begin{figure}
    \includegraphics[width=0.8\textwidth]{img/webpage}
  \end{figure}
\end{frame}



\section{References}
\begin{frame}
  \frametitle{References}
  Thank you!

  \medskip
  \bigskip
  References:
  \small
  \begin{itemize}
  \item
    Oswin \textsc{Aichholzer},
    Sergey \textsc{Bereg},
    Adrian \textsc{Dumitrescu},
    Alfredo \textsc{García},
    Clemens \textsc{Huemer},
    Ferran \textsc{Hurtado},
    Mikio \textsc{Kano},
    Alberto \textsc{Márquez},
    David \textsc{Rappaport},
    Shakhar \textsc{Smorodinsky},
    Diane L. \textsc{Souvaine},
    Jorge \textsc{Urrutia},
    David R. \textsc{Wood}.\\
    \href{https://arxiv.org/abs/0709.3375v2}{\textbf{\textit{Compatible Geometric Matchings}}}.\\
    arXiv.org, \nth{2} version, January 16, 2008
  \item
    Olivier \textsc{Pirson},\\
    \href{http://www.opimedia.be/CV/2016-2017-ULB/INFO-F420-Computational-geometry/Project-Disjoint-Compatible-Perfect-Matchings/}{\textbf{\textit{Disjoint Compatible Perfect Matchings}}}.\\
    Web page 2017,\\
    {\footnotesize
      \href{http://www.opimedia.be/CV/2016-2017-ULB/INFO-F420-Computational-geometry/Project-Disjoint-Compatible-Perfect-Matchings/}{\path|http://www.opimedia.be/CV/2016-2017-ULB/|}\\
      \href{http://www.opimedia.be/CV/2016-2017-ULB/INFO-F420-Computational-geometry/Project-Disjoint-Compatible-Perfect-Matchings/}{\path|INFO-F420-Computational-geometry/Project-Disjoint-Compatible-Perfect-Matchings/|}}
  \end{itemize}

  \bigskip
  Questions time\dots
\end{frame}

\end{document}
