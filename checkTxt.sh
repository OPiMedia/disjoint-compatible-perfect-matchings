#!/bin/sh

CHECKTXT=checkTxt.pl

OPTS='+lisible -put +questions'

REMOVE='-Fsite/favicon.ico
-E^img/
-Fsite/Disjoint-Compatible-Perfect-Matchings--INFO-F420--Olivier-Pirson-2017.pdf
-Fsite/Disjoint-Compatible-Perfect-Matchings--slides--INFO-F420--Olivier-Pirson-2017.pdf
-E^site/docs/
-E^site/public/img/
-E^site/public/ViewerJS/
-E^slides/img/'

$CHECKTXT $OPTS $REMOVE $1 $2 $3 $4 $5 $6 $7 $8 $9 * */* */*/* */*/*/* */*/*/*/*
