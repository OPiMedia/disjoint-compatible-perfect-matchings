/**
 * @file Assert functions.
 * @version March 26, 2017
 *
 * @author Olivier Pirson --- http://www.opimedia.be/
 * @license GPLv3 --- Copyright (C) 2017 Olivier Pirson
 */

/**
 * Returns true iff assertions are enabled
 * iff window.ASSERT_ENABLED is defined.
 *
 * @returns {boolean}
 */
function isAssert() {
    "use strict";

    return (window.ASSERT_ENABLED !== undefined);
}



/**
 * Assertion function.
 *
 * If its first parameter is true
 * then does nothing,
 * else prints to the console next parameters.
 *
 * If !isAssert()
 * then always does nothing.
 */
let assert = function () {};

if (isAssert()) {
    console.log("Assertions enabled!");
    assert = console.assert;  // https://developer.mozilla.org/en-US/docs/Web/API/Console/assert
}
