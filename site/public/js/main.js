/**
 * @file project.js
 * @version March 20, 2017
 *
 * @author Olivier Pirson --- http://www.opimedia.be/
 * @license GPLv3 --- Copyright (C) 2017 Olivier Pirson
 */

var globalController;


(function () {
    "use strict";

    /**
     * Create matchings, views and controllers,
     * and update views.
     */
    function _init() {
        // Left panel
        const leftMatching = new Matching();

        const leftMatchingItem = document.getElementById('left-matching');
        const leftMatchingView = new MatchingView(leftMatching, leftMatchingItem);

        const leftMatchingController = new MatchingController(leftMatching, leftMatchingView);


        // Right panel
        const rightMatching = new Matching(leftMatching);

        const rightMatchingItem = document.getElementById('right-matching');
        const rightMatchingView = new MatchingView(rightMatching, rightMatchingItem, leftMatchingView);

        const rightMatchingController = new MatchingController(rightMatching, rightMatchingView);


        // Global
        const globalView = new GlobalView(leftMatchingView, rightMatchingView, document.getElementById("global-infos-container"), document.getElementById("list-matchings-container"), document.getElementById("infos-list-matchings-container"));

        globalController = new GlobalController(globalView, leftMatchingController, rightMatchingController);

        leftMatchingController.setGlobalView(globalView);
        rightMatchingController.setGlobalView(globalView);

        globalView.update();
    }


    /* Main */
    window.addEventListener("load", _init, false);
}());
