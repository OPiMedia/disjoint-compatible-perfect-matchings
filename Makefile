.SUFFIXES:

###########
# Options #
###########
JSDOC3      = jsdoc  # http://usejsdoc.org/
JSDOC3FLAGS =

SASS      = sass  # http://sass-lang.com/
SASSFLAGS = -E utf-8

RM    = rm -f
SHELL = sh



########
# Main #
########
.PHONY: all check css docs

all:	css

check:
	./checkTxt.sh

docs:
	$(JSDOC3) $(JSDOC3FLAGS) -a all -p -d site/docs/ -r --verbose site/public/js/ site/public/js/README.md

css:	site/public/css/style.css



########
# Rule #
########
.PRECIOUS:	%.css

site/public/css/%.css:	sass/%.sass
	$(SASS) $(SASSFLAGS) --style expanded $< $@
	$(RM) $@.map



#########
# Clean #
#########
.PHONY:	clean cleanDocs distclean overclean

clean:
	$(RM) checkTxt.log
	$(RM) hs_err_*.log
	$(RM) -r .sass-cache

cleanDocs:
	$(RM) -r site/docs

distclean:	clean cleanDocs
	$(RM) site/public/css/style.css

overclean:	distclean
